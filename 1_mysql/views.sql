USE test;
DROP VIEW IF EXISTS faktury;
DROP FUNCTION IF EXISTS p0;
DROP FUNCTION if EXISTS p1;
DROP VIEW if EXISTS faktury_pozycje;

SET @pozycja=0;
SET @licznik=0;
SET @previous=NULL;

DELIMITER $$
CREATE FUNCTION p0 () 
RETURNS INTEGER DETERMINISTIC 

BEGIN 
  DECLARE dist INTEGER;
  IF(@licznik is NULL) THEN
	SET @licznik =0;
  END IF;
	SET @licznik=@licznik+1;
	SET dist = @licznik;
   RETURN dist;
END$$
DELIMITER ;



CREATE VIEW faktury AS SELECT p0() AS id, symbol, SUM(netto) AS netto, ROUND(SUM(tax*netto*0.01),2) AS podatek,
ROUND(SUM(tax*netto*0.01+netto),2) AS brutto,
COUNT(*) AS pozycji, IFNULL(SUM(CASE WHEN is_service=0 THEN 1 END),0) AS towarów,
IFNULL(SUM(CASE WHEN is_service=0 THEN netto END),0) AS towary_netto, IFNULL(ROUND(SUM(CASE WHEN is_service=0 THEN tax*netto*0.01+netto END),2),0) AS towary_brutto, IFNULL(SUM(CASE WHEN is_service=1 THEN 1 END),0) AS usług,
IFNULL(SUM(CASE WHEN is_service=1  THEN netto END),0) AS usługi_netto,
IFNULL(ROUND(SUM(CASE WHEN is_service=1 THEN tax*netto*0.01+netto END),2),0) AS usługi_brutto,
date AS data FROM invoices_position JOIN invoices on invoices.id=invoices_position.invoice_id group by invoice_id;

SET @licznik=0;
     



DELIMITER $$
CREATE FUNCTION p1 (current VARCHAR(10),symbol varchar(10)) 
RETURNS INTEGER DETERMINISTIC 

BEGIN 
  DECLARE dist INTEGER;
  IF(@pozycja is NULL) THEN
	SET @pozycja =0;
  END IF;
  
  IF(@previous is NULL) THEN
	SET @previous = symbol;
  END IF;
  
  IF(symbol <> @previous) THEN
    SET @pozycja = 1;
    ELSE
     SET @pozycja=@pozycja+1;
  END IF;

  SET dist = @pozycja;
  SET @previous = symbol;
  RETURN dist;
END$$
DELIMITER ;



DROP VIEW if EXISTS faktury_pozycje;
CREATE VIEW faktury_pozycje AS SELECT p0() AS id, invoices.id AS id_faktury, p1(23,symbol) AS pozycja, symbol AS symbol_faktury, netto, ROUND(netto+tax*netto*0.01,2) AS brutto, ROUND(
tax*netto*0.01,2) AS podatek_wartość, tax AS podatek_procent, date AS data_faktury,
(CASE WHEN is_service=1 THEN "USŁUGA" WHEN is_service=0 THEN "TOWAR" END) AS rodzaj,
name AS nazwa
FROM invoices_position
JOIN invoices on invoices.id =invoices_position.invoice_id;



