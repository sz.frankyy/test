Odpowiedzi w katalogach:
1_mysql/
2_php
3_vue2/

Aby uruchomić trzeci przykład wystarczy wpisać
```bash
npm run build
```

Podczas instalacji na pytanie: 
Use ESLint to lint your code?
proszę odpowiedzieć przecząco. Reszta domyślna.

Aby uruchomić trzeci projekt wystarczy wpisać:
```bash
cd 3_vue2 && npm run dev
```
oraz otworzyć przeglądarkę, o wpisać adres http://localhost:8080
